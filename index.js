const
  griglia = document.querySelector('#griglia'),
  barra = document.querySelector('#barra'),
  larghezza_griglia = 20,
  altezza_griglia = 20,
  colori_griglia = [],
  colori_barra = [];

pulisci();
disegna_griglia();
disegna_barra();

barra.on('click', allaga);

function allaga(evento) {
  const
    x = evento.x,
    y = evento.y,
    colore = colore_barra(x, y);

  allaga_griglia(colore);
}

function pulisci() {
  for (let riga=0; riga < altezza_griglia; riga++) {
    colori_griglia[riga] = [];
    for (let colonna=0; colonna < larghezza_griglia; colonna++) {
      colori_griglia[riga][colonna] = a_caso(['bianco, nero']);
    }
  }
}

function disegna_griglia() {

}

function disegna_barra() {

}

function allaga_griglia(colore) {

}

function colore_barra() {

}

function a_caso(lista) {
  return lista[Math.floor(Math.random() * lista.length)]
}
