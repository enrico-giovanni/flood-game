const
  renderer = new PIXI.autoDetectRenderer(800, 600, {antialias: true}),
  scena = new PIXI.Container(),
  log = console.log;

const
  SFONDO = 0x123456, BIANCO = 0xFFFFFF, NERO = 0x000000, ROSSO = 0xFF0000, VERDE = 0x00FF00, BLU = 0x0000FF,
  righe = 15,
  colonne = 20,
  posizione_griglia = {x: 150, y: 50},
  posizione_barra = {x: 20, y: 50},
  blocco_larghezza = 30,
  blocco_altezza = 30;

const
  griglia = [],
  colori = [BIANCO, NERO, VERDE];

/** imposta il colore di sfondo */
renderer.backgroundColor = SFONDO;

/** disegna la griglia */
for (let riga = 0; riga < righe; riga++) {
  griglia[riga] = [];
  for (let colonna = 0; colonna < colonne; colonna++) {
    griglia[riga][colonna] = blocco(
      posizione_griglia.x + colonna * blocco_larghezza,
      posizione_griglia.y + riga * blocco_altezza,
      a_caso(colori)
    );
  }
}

/** disegna la barra */
colori
  .forEach((colore, indice) =>
    blocco(
      posizione_barra.x,
      posizione_barra.y + indice * blocco_altezza * 2,
      colore,
      true
    )
  );

/** anima il gioco */
game_loop();

/** ---------------------------------------------------------------- */

/** funzione che viene chiamata 60 volte al secondo */
function game_loop() {
  requestAnimationFrame(game_loop);
  renderer.render(scena);
}

/** torna un elemento a caso della lista */
function a_caso(lista) {
  const
    numero_a_caso = Math.random(),
    indice_a_caso = Math.floor(numero_a_caso * lista.length);
  return lista[indice_a_caso]
}

/** torna un nuovo sprite già posizionato e colorato */
function blocco(x, y, colore, barra = false) {
  const g = new PIXI.Graphics();
  g.lineStyle(2, BLU, 1);
  g.beginFill(colore, 1);
  g.drawRoundedRect(0, 0, blocco_larghezza * (barra ?2 :1), blocco_altezza * (barra ?2 :1), 1);
  g.endFill();

  const sprite = new PIXI.Sprite(g.generateCanvasTexture(PIXI.SCALE_MODES.LINEAR));
  sprite.id = uuid.v1();
  sprite.colore = colore;

  sprite.interactive = barra;
  sprite.position.x = x;
  sprite.position.y = y;
  scena.addChild(sprite);

  if (barra) {
    function touch() {
      /** azzera il segnale di visitato */
      for (let riga = 0; riga < righe; riga++)
        for (let colonna = 0; colonna < colonne; colonna++)
          griglia[riga][colonna].visitato = false;

      flood(0, 0, griglia[0][0].colore, sprite.colore);
    }
    sprite.on('mousedown', touch);
    sprite.on('touchend', touch);
  }

  return sprite
}

/** allaga la casella (riga, colonna) dal colore ... al colore ...*/
function flood(riga, colonna, da_colore, a_colore) {
  if (riga < 0 || colonna < 0 || riga >= righe || colonna >= colonne)  /** fuori margine */
    return;
  if (griglia[riga][colonna].colore === a_colore)   /** già dello stesso colore */
    return;

  /** percorre i non visitati del colore della casella 0,0 */
  if (griglia[riga][colonna].colore === da_colore && !griglia[riga][colonna].visitato) {
    griglia[riga][colonna].visitato = true;
    flood(riga, colonna + 1, da_colore, a_colore);
    flood(riga, colonna - 1, da_colore, a_colore);
    flood(riga + 1, colonna, da_colore, a_colore);
    flood(riga - 1, colonna, da_colore, a_colore);
    sostituisci(riga, colonna, a_colore)
  }
}

/** sostituisce lo sprite a (riga, colonna) con uno del colore dato*/
function sostituisci(riga, colonna, colore) {
  const
    vecchio = griglia[riga][colonna],
    nuovo = blocco(vecchio.position.x, vecchio.position.y, colore);

  scena.removeChild(vecchio);
  griglia[riga][colonna] = nuovo;
}
