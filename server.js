const
  basePath = __dirname,
  http = require('http'),
  fs = require('fs'),
  path = require('path');

http.createServer(function(req, res) {
    const stream = fs.createReadStream(path.join(basePath, req.url));
    stream.on('error', function() {
        res.writeHead(404);
        res.end();
    });
    stream.pipe(res);
}).listen(3000);
